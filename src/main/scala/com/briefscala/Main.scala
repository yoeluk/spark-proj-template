package com.briefscala

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkContext, SparkConf}

object Main {
  def main(args: Array[String]) {
    val conf = new SparkConf()
      .setAppName("GitHub push counter")
      .setMaster("local[*]")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
  }
}
